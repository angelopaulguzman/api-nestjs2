import { Prop, SchemaFactory, Schema } from "@nestjs/mongoose";
import { CaracteristicDto } from "../dto/caracteristics.dto";

export type CoinDocument = Coin & Document;

@Schema()
export class Coin {
    @Prop({ required: true })
    name: string;
    @Prop()
    alias: string;
    @Prop()
    money: number;
    @Prop()
    undermine: string;
    @Prop()
    readonly caracteristics: CaracteristicDto[];
}

export const CoinSchema = SchemaFactory.createForClass(Coin);