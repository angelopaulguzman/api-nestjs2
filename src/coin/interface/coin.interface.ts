import { CaracteristicDto } from './../dto/caracteristics.dto';
import { Document } from "mongoose";
export interface CoinInterface extends Document{
    readonly name: string;
    readonly alias: string;
    readonly money: number;
    readonly undermine: string;

    readonly caracteristics: CaracteristicDto[];
}