import { CoinDto } from './dto/coin.dto';
import { CoinService } from './coin.service';
import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Put, Query, Res } from '@nestjs/common';

@Controller('coin')
export class CoinController {
    constructor(private coinService: CoinService ) {}

    @Get('/')
    async getAllCoin(@Res() res){
        const coins = await this.coinService.getCoins();
        return res.status(HttpStatus.OK).json(coins);
    }

    @Post('/')
    async createCoin(@Res() res, @Body() coin: CoinDto){
        try {
            const coinRespone = await this.coinService.createCoin(coin);
            return res
          .status(HttpStatus.ACCEPTED)
          .json({ message: 'la criptomoneda fue creada' });
      } catch (e) {
        return res
          .status(HttpStatus.BAD_REQUEST)
          .json({ message: e.errors.name.message });
      }
    }

    @Get('/:coinId')
    async getCoinById(@Res() res, @Param('coinId') coinId: string){
        if ( coinId.length !==24)
        return res
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'la criptomoneda no existe joven pruebe de nuevo'});
    const coin = await this.coinService.getCoinById(coinId);
    if (!coin)
    return res
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'la criptomoneda no existe joven pruebe de nuevo'});
        return res.status(HttpStatus.OK).json(coin);
    }

    @Delete('delete')
    async deleteCoin(@Res() res, @Query('id') coinId: string){
        if(coinId.length !== 24)
        return res
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'la criptomoneda no existe joven pruebe de nuevo'});
    const coinDeleted = await this.coinService.deleteCoinById(coinId);
    if (!coinDeleted)
    return res
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'la criptomoneda no existe joven pruebe de nuevo'});
    return res
        .status(HttpStatus.ACCEPTED)
        .json({ message: 'la criptomoneda ha sido eliminado con exito'});
    }

    @Delete('/:coinId')
    async deleteCoinById(@Res() res, @Param('coinId') coinId: string){
    if (coinId.length !== 24)
    return res
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'la criptomoneda no existe'});
    const coinDeleted = await this.coinService.deleteCoinById(coinId);
    if (!coinDeleted)
    return res
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'la criptomoneda no existe joven pruebe de nuevo'});
    return res
        .status(HttpStatus.ACCEPTED)
        .json({ message: 'la criptomoneda ha si eliminada'});
    }

    @Put('/:coinId')
    async editCoin(@Res() res, @Param('coinId') coinId: string, @Body() coin: CoinDto){
    if (coinId.length !== 24)
    return res
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'la criptomoneda no existe'});
    const updateCoin = await this.coinService.updateCoin( coinId, coin);
    if (!updateCoin)
    return res
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'la criptomoneda no existe' });
    return res
        .status(HttpStatus.ACCEPTED)
        .json({ message: 'la criptomoneda se edito con exito' });
    }
}
