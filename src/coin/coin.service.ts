import { CoinDto } from './dto/coin.dto';
import { CoinDocument } from './schema/coin.schema';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class CoinService {
    constructor(
        @InjectModel('Coin') readonly coinModel: Model<CoinDocument>,
    ) {}

    async getCoins(): Promise<CoinDocument[]>{
        const Coins = await this.coinModel.find();
        return Coins;
    }

    async getCoinById(coinId: string): Promise<CoinDocument>{
        const coin = await this.coinModel.findById(coinId);
        return coin;
    }

    async createCoin(coinDto: CoinDto): Promise<CoinDto> {
        const newCoin = new this.coinModel(coinDto);
        return newCoin.save();
    }

    async deleteCoinById(coinId: string): Promise<any> {
        const deletedCoin = await this.coinModel.findByIdAndDelete(coinId);
        return deletedCoin;
    }

    async updateCoin(coinId: string, coin: CoinDto): Promise<any>{
        const updateCoin = await this.coinModel.findByIdAndUpdate(coinId, coin);
        return updateCoin;
    }

}