import { CaracteristicDto } from './caracteristics.dto';
export class CoinDto{
    readonly name: string;
    readonly alias: string;
    readonly money: number;
    readonly undermine: string;

    readonly caracteristics: CaracteristicDto[];
}