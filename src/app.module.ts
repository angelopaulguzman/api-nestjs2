import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CoinModule } from './coin/coin.module';

@Module({
  imports: [
    CoinModule,
    MongooseModule.forRoot('mongodb://localhost:/coinnestjs'), CoinModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
